#undef _UNICODE
#define GMMODULE
#define private public // Please dont tell the ISO I'm doing this

#ifdef POSIX
	#define LINUX
	#define _LINUX
	#define GNUC
	#define vsprintf_s( ... ) vsprintf( __VA_ARGS__ )
	#define Sleep( ... ) usleep( __VA_ARGS__ )
	#define AllocConsole()
	#define _cprintf( ... ) printf( __VA_ARGS__ )
	#define VSTD_LIB "libvstdlib.so" // /proc/pid/maps
	#include <unistd.h>
#elif defined( _WIN32 )
	#define VSTD_LIB "vstdlib.dll"
	#include <Windows.h>
	#include <conio.h>
	#pragma comment (linker, "/NODEFAULTLIB:libcmt")
#else
	#error "Unknown OS."
#endif

#include "GarrysMod/Lua/Interface.h"
#include <cstdio>
#include <cstdlib>
#include <convar.h>

using namespace GarrysMod::Lua;
char FormattedText[2048]; // Char array to hold our formatted text.

int convarMetatableRef = NULL;
lua_State* g_LuaState = NULL;
ICvar *g_ICVar = NULL;

static ConCommand *fgt_lua_run = NULL;
static ConCommand *fgt_openscript = NULL;

// Internal function for printing to the game console. Like print( string.format() )
int con_printf( lua_State* state, const char* formatString, ... ){ // Be careful about the state.
	//_cprintf( "con_printf entry stack size %i\n", LUA->Top() );
	
	va_list formatArgs; // Use <cstdio>'s varargs to pass N arguments to vsprintf()
	va_start( formatArgs, formatString );
	vsprintf_s( FormattedText, formatString, formatArgs );
	va_end( formatArgs );

	unsigned int length = strlen( FormattedText );
	LUA->PushSpecial( SPECIAL_GLOB );	// _G
		LUA->GetField( -1, "print" );	// _G.print
		LUA->PushString( FormattedText, length );	// first argument
		LUA->Call( 1, 0 );				// _G.print( arg1 );
	LUA->Pop();							// Pop _G
	
	//_cprintf( "con_printf exit stack size %i\n", LUA->Top() );
	return 0;							// No arguments to return
}

// New GetConVar global function
int getConvar( lua_State* state ){
	LUA->CheckType(1, Type::STRING);

	ConVar *cvar = g_ICVar->FindVar( LUA->GetString( 1 ) );
	
	if (cvar) {
		UserData* ud = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		ud->data = cvar;
		ud->type = Type::CONVAR;
		LUA->ReferencePush( convarMetatableRef );
		LUA->SetMetaTable( -2 ); // Set _R.ConVar as the metatable for our userdata
		return 1;
	}
	return 0;
}

// Metatable function for changing the name of a convar
int convarSetName( lua_State* state ){
	LUA->CheckType( 1, Type::CONVAR );
	LUA->CheckType( 2, Type::STRING );

	UserData* ud = (UserData*)LUA->GetUserdata( 1 );
	ConVar *cvar = (ConVar*)ud->data;

	if (!cvar)
		con_printf( state, "Invalid ConVar!" );

	cvar->m_pszName = new char[50];
	strcpy( (char*)cvar->m_pszName, LUA->GetString( 2 ) );

	return 0;
}

// Metatable function for setting the flags of a convar
int convarSetFlags( lua_State* state ){
	LUA->CheckType( 1, Type::CONVAR );
	LUA->CheckType( 2, Type::NUMBER );

	UserData* ud = (UserData*)LUA->GetUserdata( 1 );
	ConVar *cvar = (ConVar*)ud->data;

	if (!cvar)
		con_printf( state, "Invalid ConVar!" );

	cvar->m_nFlags = LUA->GetNumber( 2 );

	return 0;
}

// Metatable function for setting the value of a convar
int convarSetValue( lua_State* state ){
	LUA->CheckType( 1, Type::CONVAR );

	int argType = LUA->GetType( 2 );

	UserData* ud = (UserData*)LUA->GetUserdata( 1 );
	ConVar *cvar = (ConVar*)ud->data;

	if (!cvar)
		con_printf( state, "Invalid ConVar!" );

	switch( argType ){
		case Type::NUMBER:
			cvar->SetValue( (float) LUA->GetNumber( 2 ) );
			break;
		case Type::BOOL:
			cvar->SetValue( LUA->GetBool( 2 ) ? 1 : 0 );
			break;
		case Type::STRING:
			cvar->SetValue( LUA->GetString( 2 ) );
			break;
		default:
			con_printf( state, "Argument #2 invalid!" );
			break;
	}

	return 0;
}

// Metatable function for getting a convars flags as integer
int convarGetFlags( lua_State* state ){
	LUA->CheckType( 1, Type::CONVAR );

	UserData* ud = (UserData*)LUA->GetUserdata( 1 );
	ConVar *cvar = (ConVar*)ud->data;
	
	if (!cvar)
		con_printf( state, "Invalid ConVar!" );

	LUA->PushNumber( (float)cvar->m_nFlags );

	return 1;
}

// lua_openscript_cl RunString( args )
void fgtOpenscript( const CCommand &command ){
	lua_State* state = g_LuaState;
	LUA->PushSpecial( SPECIAL_GLOB );
		LUA->GetField( -1, "include" );
		LUA->PushString( command.ArgS() );
		LUA->Call( 1, 0 );
	LUA->Pop();
}

// lua_run_cl include( args )
void fgtRunLua( const CCommand &command ){
	//con_printf( g_LuaState, "fgt_lua_run called! [%s]", command.ArgS() );
	lua_State* state = g_LuaState;
	LUA->PushSpecial( SPECIAL_GLOB );
		LUA->GetField( -1, "RunString" );
		LUA->PushString( command.ArgS() );
		LUA->Call( 1, 0 );
	LUA->Pop();
}

GMOD_MODULE_OPEN(){
	//AllocConsole();
	bool shouldFullyLoad = false;
	g_LuaState = state;
	LUA->PushSpecial( SPECIAL_GLOB ); // If DEBUGFGTCVAR = true then AllocConsole()
		LUA->GetField( -1, "DEBUGFGTCVAR" );
		if( LUA->GetType( -1 ) == Type::BOOL ){
			bool allocDebugCon = LUA->GetBool( -1 );
			if( allocDebugCon ){
				AllocConsole();
				Sleep( 250 ); // Give some time for the console to allocate.
			}
		}
		LUA->Pop(); // DEBUGFGTCVAR
		LUA->GetField( -1, "FGTCONVARSUPERSECRETENABLE" ); // Super secret global to actually enable this module. (Prevents anti-cheats that load all modules and try :GetFlags())
		if( LUA->GetType( -1 ) == Type::BOOL ){
			shouldFullyLoad = LUA->GetBool( -1 );
		}
		if( !shouldFullyLoad ){
			con_printf( state, "##########################################################\n\tFgt Convar tried to load without FGTCONVARSUPERSECRETENABLE set!\n##########################################################" );
		}
		LUA->Pop(); // FGTCONVARSUPERSECRETENABLE
	LUA->Pop(); // _G
	_cprintf( "\n\n\n\n\n\nGMOD_MODULE_OPEN\n" );
	_cprintf( "GMOD_MODULE_OPEN Entered with %i objects on the stack.\n", LUA->Top() );

	CreateInterfaceFn LibFactory = Sys_GetFactory( VSTD_LIB );
	if( !LibFactory ){
		con_printf( state, "Can't create factory from vstdlib.dll!" );
	} else {
		g_ICVar = (ICvar*)LibFactory( CVAR_INTERFACE_VERSION, 0 );
		if( !g_ICVar ){
			con_printf( state, "Error getting ICvar interface!" );
			con_printf( state, "All console commands are broken!" );
		} else {
			fgt_lua_run = new ConCommand( "fgt_lua_run", fgtRunLua, "", FCVAR_UNREGISTERED );
			g_ICVar->RegisterConCommand( fgt_lua_run );

			fgt_openscript = new ConCommand( "fgt_openscript", fgtOpenscript, "", FCVAR_UNREGISTERED );
			g_ICVar->RegisterConCommand( fgt_openscript );
		}
	}

	if( shouldFullyLoad ){ // Only add _R.ConVar methods if we have the SECRET PHRASE set.
		LUA->PushSpecial( SPECIAL_REG ); // Push the registry onto the stack _R
		LUA->GetField( -1, "ConVar" ); // _R ConVar
			convarMetatableRef = LUA->ReferenceCreate(); // Store a reference to the ConVar metatable (this pops it off the stack)
			LUA->ReferencePush( convarMetatableRef ); // Push it back onto the stack.
			LUA->PushCFunction( convarSetName );
			LUA->SetField( -2, "FgtSetName" );

			LUA->PushCFunction( convarSetFlags );
			LUA->SetField( -2, "FgtSetFlags" );

			LUA->PushCFunction( convarSetValue );
			LUA->SetField( -2, "FgtSetValue" );

			/*LUA->PushCFunction( convarGetInt );
			LUA->SetField( -2, "FgtGetInt" );*/
		
			LUA->PushCFunction( convarGetFlags );
			LUA->SetField( -2, "FgtGetFlags" );
		LUA->Pop( 2 ); // pop registry and ConVar tables
	}
	
	_cprintf( "GMOD_MODULE_OPEN Exited with %i objects on stack.\n", LUA->Top() );
	return 0;
}

GMOD_MODULE_CLOSE(){
	_cprintf( "GMOD_MODULE_CLOSE (%i objects on stack)\n", LUA->Top() );

	if( convarMetatableRef != NULL )
		LUA->ReferenceFree( convarMetatableRef );

	if( fgt_lua_run != NULL ){
		g_ICVar->UnregisterConCommand( fgt_lua_run );
		delete fgt_lua_run;
	}

	if( fgt_openscript != NULL ){
		g_ICVar->UnregisterConCommand( fgt_openscript );
		delete fgt_openscript;
	}

	//MessageBox( NULL, "GMOD_MODULE_CLOSE", NULL, MB_OK );
	return 0;
}
