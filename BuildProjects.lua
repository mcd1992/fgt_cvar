solution "gmcl_fgt_cvar"

	language "C++"
	location ( os.get() .."-".. _ACTION )
	flags { "Symbols", "NoEditAndContinue", "NoPCH", "StaticRuntime", "EnableSSE" }
	targetdir ( "lib/" .. os.get() .. "/" )
	includedirs {	"/home/unknown/Development/GmodModules/gmod-module-base/include",
			"/home/unknown/Development/source-sdk-2013/mp/src/public",
			"/home/unknown/Development/source-sdk-2013/mp/src/public/tier1"
		    }
	libdirs { "/home/unknown/Development/source-sdk-2013/mp/src/lib/public/linux32" }
	configurations
	{ 
		"Release"
	}
	
	configuration "Release"
		defines { "NDEBUG" }
		flags{ "Optimize", "FloatFast" }
	
	project "gmcl_fgt_cvar_linux"
		defines { "POSIX" }
		links { "tier0", ":tier1.a" }
		files { "src/**.*", "/home/unknown/Development/GmodModules/gmod-module-base/include/**.*" }
		kind "SharedLib"
		
